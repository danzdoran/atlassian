package com.atlassian.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * On Sunday February 10, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-10.
 */
@Data
@AllArgsConstructor
public class SumResponse {
    private String message;
}
