package com.atlassian.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * On Saturday February 09, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-09.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Story {
    private String issueKey;
    private Fields fields;

}
