package com.atlassian.demo.service;

import com.atlassian.demo.model.Fields;
import com.atlassian.demo.model.Story;
import com.atlassian.demo.model.Total;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * On Saturday February 09, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-09.
 */
@Slf4j
@Service
public class JiraService {

    @Value("${jira.base.url}")
    private String jiraUrl;

    private RestTemplate restTemplate;
    private SqsService sqsService;

    public JiraService(RestTemplate restTemplate, SqsService sqsService) {
        this.restTemplate = restTemplate;
        this.sqsService = sqsService;
    }

    public Story[] getStories(String query) {
        ResponseEntity<Story[]> response
                = restTemplate.getForEntity(jiraUrl + "/rest/api/2/search?q=" + query, Story[].class);
        log.info("Response: {}", response.getBody());
        return response.getBody();
    }

    public Total getTotal(Story[] stories, String name) {

        int sum = Arrays.stream(stories)
                .map(Story::getFields)
                .map(Fields::getStoryPoints)
                .mapToInt(Integer::intValue)
                .sum();

        return new Total(name, sum);

    }

    public boolean getTotalAndSendToSqs(String query, String name) {
        Story[] stories = getStories(query);
        Total total = getTotal(stories, name);
        try {
            sendTotalToSqs(total);
        } catch (JsonProcessingException e) {
            log.error("Error sending message to SQS: {}", e);
            return false;
        }
        return true;
    }

    private void sendTotalToSqs(Total total) throws JsonProcessingException {
        sqsService.sendTotalMessage(total);
    }

}
