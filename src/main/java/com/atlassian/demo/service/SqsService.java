package com.atlassian.demo.service;

import com.atlassian.demo.model.Total;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

/**
 * On Saturday February 09, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-09.
 */
@Slf4j
@Service
public class SqsService {

    @Value("${queue.name}")
    private String queueName;

    private JmsTemplate defaultJmsTemplate;

    private ObjectMapper objectMapper;

    public SqsService(ObjectMapper objectMapper, JmsTemplate defaultJmsTemplate) {
        this.defaultJmsTemplate = defaultJmsTemplate;
        this.objectMapper = objectMapper;
    }

    public void sendTotalMessage(Total total) throws JsonProcessingException {
        log.info("Sending total: {} to SQS", total);
        String s = objectMapper.writeValueAsString(total);
        defaultJmsTemplate.convertAndSend(queueName, s);
    }
}
