package com.atlassian.demo.controller;

import com.atlassian.demo.model.SumResponse;
import com.atlassian.demo.service.JiraService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * On Saturday February 09, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-09.
 */
@Slf4j
@RestController
public class SumController {

    @Autowired
    JiraService jiraService;

    @GetMapping("/api/issue/sum")
    public ResponseEntity<SumResponse> getSum(@RequestParam("query") String query, @RequestParam("name") String name) {
        log.info("Query: {}", query);
        log.info("Name: {}", name);
        if (jiraService.getTotalAndSendToSqs(query, name))
            return ResponseEntity.ok(new SumResponse("Successfully sent message"));
        else
            return new ResponseEntity<>(new SumResponse("Error message not sent"), HttpStatus.BAD_REQUEST);
    }

}
