package com.atlassian.demo;

import com.atlassian.demo.model.Fields;
import com.atlassian.demo.model.Story;
import com.atlassian.demo.model.Total;
import com.atlassian.demo.service.JiraService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * On Sunday February 10, 2019 this glorious class was created
 * Created by Daniel Doran 2019-02-10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SumTest {

    @Autowired
    JiraService jiraService;

    private String givenName = "cst";
    private Story[] givenStories = {new Story("TEST-1", new Fields(1)),
            new Story("TEST-2", new Fields(2))};
    private Total expectedTotal = new Total("cst", 3);

    @Test
    public void testSum() {
        Total total = jiraService.getTotal(givenStories, givenName);
        Assert.assertEquals(total, expectedTotal);
    }

}
